
# coding: utf-8

# In[7]:


import pandas as pd
import matplotlib.pyplot as plt


# In[212]:


df=pd.read_csv('C:\\Machstatz\\DATA CLEANING\\DIG-FEB.csv')


# In[213]:


df=df.drop(df.index[170:174])


# In[214]:


df=df.drop('UTILITY /PROBLEM',1)


# In[215]:


df=df.drop(['Unnamed: 21','Unnamed: 22','Unnamed: 23','S/A B/D TANK L/H','WW B/D TANK L/H','slno','BATCH NO','ELAPSD TIME'],1)


# In[216]:


df=df.drop(['DIG START DELAY','REACTION DELAY','M/M','ACID PREPARATION','ACID FILLING IN OTHER DIG','WASH WATER FILLING DELAY'],1)


# In[217]:


df=df.drop(['PROCESS DELAY','Unnamed: 19','Unnamed: 20'],1)


# In[219]:


new=df


# In[222]:


new=new.dropna()


# In[223]:


new


# In[224]:


from pandas import ExcelWriter

writer = ExcelWriter('C:\\Machstatz\\dig_feb.xlsx')
new.to_excel(writer,'Sheet5')
writer.save()


# In[228]:


new=pd.read_csv('C:\\Machstatz\\dig_feb.csv')


# In[232]:


pd.to_datetime(new['START'])
pd.to_datetime(new['Unnamed: 4'])


# In[233]:


pd.to_datetime(new['END'])
pd.to_datetime(new['Unnamed: 6'])


# In[234]:


df['START']=df[['START','Unnamed: 4']].apply(lambda x: ' '.join(x),axis=1)


# In[235]:



new['Unnamed: 6']=new['Unnamed: 6'].astype(str)


# In[236]:


new['Unnamed: 4']=new['Unnamed: 4'].astype(str)


# In[237]:


new['END']=new['END'].astype(str)


# In[238]:


new['START']=new['START'].astype(str)


# In[242]:


new['START']=new[['START','Unnamed: 4']].apply(lambda x:' '.join(x),axis=1)


# In[243]:


new['END']=new[['END','Unnamed: 6']].apply(lambda x:' '.join(x),axis=1)


# In[245]:


pd.to_datetime(new['START'])


# In[246]:


pd.to_datetime(new['END'])


# In[253]:


new


# In[254]:


from pandas import ExcelWriter

writer = ExcelWriter('C:\\Machstatz\\dig_feb_final.xlsx')
new.to_excel(writer,'Sheet5')
writer.save()


# In[185]:


##UPTO NOW THE DATA CLEANING IS OVER...


# In[269]:





# In[404]:


g=new.groupby("DIG")


# In[405]:


for dig_df in g:
    print(dig_df)


# In[471]:


li=[]
my_set=set(new['DIG'])
li=list(my_set)

    


# In[472]:


li


# In[511]:


x=0
df={}
for dig,dig_df in g:
    if x <len(li):
        df['{}'.format(dig)]=pd.DataFrame(g.get_group(dig))
    x+=1


# In[512]:


df['A']


# In[510]:


#import numpy  (splitting of dataset into training and testing)
# x is your dataset
#x = numpy.random.rand(100, 5)
#training_idx = numpy.random.randint(x.shape[0], size=80)
#test_idx = numpy.random.randint(x.shape[0], size=20)
#training, test = x[training_idx,:], x[test_idx,:]

